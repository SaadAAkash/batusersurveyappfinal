package com.androidadvance.androidsurvey;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
/**
 * Created by interspeed.com.bd on 2/28/2018.
 */

public class LastActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last );
    }
}

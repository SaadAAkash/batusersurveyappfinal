package com.androidadvance.androidsurvey.listener;

import com.androidadvance.androidsurvey.model.BaseFormElement;

/**
 * Callback to activity when any data in form adapter is changed
 */

public interface OnFormElementValueChangedListener {

    void onValueChanged(BaseFormElement baseFormElement);

}
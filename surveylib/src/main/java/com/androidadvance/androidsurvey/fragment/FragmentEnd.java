package com.androidadvance.androidsurvey.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.androidsurvey.Answers;
import com.androidadvance.androidsurvey.MyCameraActivity;
import com.androidadvance.androidsurvey.MyCameraActivity3;
import com.androidadvance.androidsurvey.R;
import com.androidadvance.androidsurvey.SurveyActivity;
import com.androidadvance.androidsurvey.models.SurveyProperties;

import com.androidadvance.androidsurvey.MyCameraActivity;

import org.json.JSONObject;

import java.net.URL;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;

public class FragmentEnd extends Fragment {

    private FragmentActivity mContext;
    private TextView textView_end;
    private String jsonString;
    public JSONObject obj;

    public String status;

    URL url;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_end, container, false);


        Button button_finish = (Button) rootView.findViewById(R.id.button_finish);
        textView_end = (TextView) rootView.findViewById(R.id.textView_end);


        button_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /**
                 * IMPORTANT:: SEE THE NEXT LINE, RETURNS THE RESULTS
                 */

                //((SurveyActivity) mContext).event_survey_completed(Answers.getInstance());

                jsonString = Answers.getInstance().get_json_object();
                try {

                    obj = new JSONObject(jsonString);
                    Toast.makeText(getContext(), obj.toString() , Toast.LENGTH_LONG).show();
                    new SendRequest().execute();

                    //Toast.makeText(getContext(), status, Toast.LENGTH_LONG).show();
                } catch (Throwable tx) {

                    Log.e("My App", "Could not parse malformed JSON: \"" + jsonString + "\"");
                }

                //((MyCameraActivity) cameraContext).event_survey_completed(Answers.getInstance());

                Intent i = new Intent( getContext(), MyCameraActivity.class);
                startActivity(i);

            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mContext = getActivity();
        SurveyProperties survery_properties = (SurveyProperties) getArguments().getSerializable("survery_properties");

        assert survery_properties != null;
        textView_end.setText(Html.fromHtml(survery_properties.getEndMessage()));

    }


    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    ////////////////////////////////////////////
    public class SendRequest extends AsyncTask<String, Void, String>
    {


        protected void onPreExecute(){}

        protected String doInBackground(String... arg0) {

            try{

                //URL url = new URL("http://batcon.seedemo.space/test.php");
                //URL url = new URL("http://342dfcb2.ngrok.io/info");

                URL url = new URL("http://batcon.clientdemo.club/info");

                //JSONObject postDataParams = new JSONObject();


                //postDataParams.put("name", "abhay");
                //postDataParams.put("email", "abhay@gmail.com");

                //Log.e("params",postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(obj));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK)
                {

                    BufferedReader in=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();

                   // Toast.makeText(getContext(), "OKKKKK " + status, Toast.LENGTH_LONG).show();

                   // status = sb.toString();
                    return sb.toString();


                }
                else {
                    //status = "false " + responseCode;
                    return new String("false : "+responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }
        }

        protected void onPostExecute(String result) {
            Toast.makeText(getContext(), result,
                    Toast.LENGTH_LONG).show();

        }
    }
    ////////////////////////////////////////////
}
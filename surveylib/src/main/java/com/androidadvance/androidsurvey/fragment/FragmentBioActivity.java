package com.androidadvance.androidsurvey.fragment;

/**
 * Created by interspeed.com.bd on 2/14/2018.
 */
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.androidadvance.androidsurvey.R;
import com.androidadvance.androidsurvey.SurveyActivity;

import java.util.ArrayList;
import java.util.List;

import me.riddhimanadib.formmaster.FormBuilder;
import me.riddhimanadib.formmaster.listener.OnFormElementValueChangedListener;
import me.riddhimanadib.formmaster.model.BaseFormElement;
import me.riddhimanadib.formmaster.model.FormElementPickerDate;
import me.riddhimanadib.formmaster.model.FormElementPickerMulti;
import me.riddhimanadib.formmaster.model.FormElementPickerSingle;
import me.riddhimanadib.formmaster.model.FormElementPickerTime;
import me.riddhimanadib.formmaster.model.FormElementSwitch;
import me.riddhimanadib.formmaster.model.FormElementTextEmail;
import me.riddhimanadib.formmaster.model.FormElementTextMultiLine;
import me.riddhimanadib.formmaster.model.FormElementTextNumber;
import me.riddhimanadib.formmaster.model.FormElementTextPassword;
import me.riddhimanadib.formmaster.model.FormElementTextPhone;
import me.riddhimanadib.formmaster.model.FormElementTextSingleLine;
import me.riddhimanadib.formmaster.model.FormHeader;

import static com.androidadvance.androidsurvey.model.BaseFormElement.TYPE_EDITTEXT_EMAIL;

public class FragmentBioActivity extends Fragment {

        private RecyclerView mRecyclerView;
        private FormBuilder mFormBuilder;

        private FragmentActivity mContext;

        private Button button_continue2;

        private TextView textview_q_title;
        private EditText editText_answer;

        private BaseFormElement formElement;



    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState)
        {
            ViewGroup rootView = (ViewGroup) inflater.inflate(
                    R.layout.activity_bio, container, false);

            button_continue2 = (Button) rootView.findViewById(R.id.button_continue);
            textview_q_title = (TextView) rootView.findViewById(R.id.textview_q_title);;
            mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
            button_continue2.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {

                   //formElement = mFormBuilder.getFormElement(TYPE_EDITTEXT_EMAIL);

                   Toast.makeText(getContext(), "CLICKED" /*+ formElement.getValue()*/, Toast.LENGTH_LONG).show();
                   ((SurveyActivity) mContext).go_to_next();
               }
            });

            return rootView;
        }
    /*
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_bio);

            setupToolBar();

            setupForm();
        }
        */
        /*@Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case android.R.id.home:
                    onBackPressed();
                    return true;
            }
            return super.onOptionsItemSelected(item);
        }

        private void setupToolBar() {

            final ActionBar actionBar = getSupportActionBar();

            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setHomeButtonEnabled(true);
            }

        }*/

       @Override
       public void onActivityCreated(Bundle savedInstanceState) {
           super.onActivityCreated(savedInstanceState);

           mContext = getActivity();

           mFormBuilder = new FormBuilder(getContext(), mRecyclerView);

           FormHeader header1 = FormHeader.createInstance("Personal Info");
           FormElementTextEmail element11 = FormElementTextEmail.createInstance().setTitle("Email").setValue("sth@sth.com");
           FormElementTextPhone element12 = FormElementTextPhone.createInstance().setTitle("Phone").setValue("+880**********");

           FormHeader header2 = FormHeader.createInstance("Family Info");
           FormElementTextSingleLine element21 = FormElementTextSingleLine.createInstance().setTitle("Location").setValue("Dhaka");
           FormElementTextMultiLine element22 = FormElementTextMultiLine.createInstance().setTitle("Address");
           FormElementTextNumber element23 = FormElementTextNumber.createInstance().setTitle("Zip Code").setValue("1200");

           FormHeader header3 = FormHeader.createInstance("Schedule");
           FormElementPickerDate element31 = FormElementPickerDate.createInstance().setTitle("DOB").setDateFormat("MMM dd, yyyy");
           FormElementPickerTime element32 = FormElementPickerTime.createInstance().setTitle("Time").setTimeFormat("KK hh");
           FormElementTextPassword element33 = FormElementTextPassword.createInstance().setTitle("Password").setValue("abcd1234");

           /*
           FormHeader header4 = FormHeader.createInstance("Preferred Items");
           List<String> fruits = new ArrayList<>();
           fruits.add("Banana");
           fruits.add("Orange");
           fruits.add("Mango");
           fruits.add("Guava");
           FormElementPickerSingle element41 = FormElementPickerSingle.createInstance().setTitle("Single Item").setOptions(fruits).setPickerTitle("Pick any item");
           FormElementPickerMulti element42 = FormElementPickerMulti.createInstance().setTitle("Multi Items").setOptions(fruits).setPickerTitle("Pick one or more").setNegativeText("reset");
           //FormElementSwitch element43 = FormElementSwitch.createInstance().setTitle("Frozen?").setSwitchTexts("Yes", "No");

            */

           List<BaseFormElement> formItems = new ArrayList<>();
           formItems.add(header1);
           formItems.add(element11);
           formItems.add(element12);
           formItems.add(header2);
           formItems.add(element21);
           formItems.add(element22);
           formItems.add(element23);
           formItems.add(header3);
           formItems.add(element31);
           formItems.add(element32);
           formItems.add(element33);

           /*
           formItems.add(header4);

           formItems.add(element41);
           formItems.add(element42);
            */

           //formItems.add(element43);

           mFormBuilder.addFormElements(formItems);
           button_continue2.setVisibility(View.VISIBLE);
           /*if (at_leaset_one_checked) {
               button_continue.setVisibility(View.VISIBLE);
           } else {
               button_continue.setVisibility(View.GONE);
           }*/


       }
}


        /*
        private void setupForm() {

            mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
            mFormBuilder = new FormBuilder(this, mRecyclerView, this);

            FormHeader header1 = FormHeader.createInstance("Personal Info");
            FormElementTextEmail element11 = FormElementTextEmail.createInstance().setTitle("Email").setHint("Enter Email");
            FormElementTextPhone element12 = FormElementTextPhone.createInstance().setTitle("Phone").setValue("+8801712345678");

            FormHeader header2 = FormHeader.createInstance("Family Info");
            FormElementTextSingleLine element21 = FormElementTextSingleLine.createInstance().setTitle("Location").setValue("Dhaka");
            FormElementTextMultiLine element22 = FormElementTextMultiLine.createInstance().setTitle("Address");
            FormElementTextNumber element23 = FormElementTextNumber.createInstance().setTitle("Zip Code").setValue("1000");

            FormHeader header3 = FormHeader.createInstance("Schedule");
            FormElementPickerDate element31 = FormElementPickerDate.createInstance().setTitle("Date").setDateFormat("MMM dd, yyyy");
            FormElementPickerTime element32 = FormElementPickerTime.createInstance().setTitle("Time").setTimeFormat("KK hh");
            FormElementTextPassword element33 = FormElementTextPassword.createInstance().setTitle("Password").setValue("abcd1234");

            FormHeader header4 = FormHeader.createInstance("Preferred Items");
            List<String> fruits = new ArrayList<>();
            fruits.add("Banana");
            fruits.add("Orange");
            fruits.add("Mango");
            fruits.add("Guava");
            FormElementPickerSingle element41 = FormElementPickerSingle.createInstance().setTitle("Single Item").setOptions(fruits).setPickerTitle("Pick any item");
            FormElementPickerMulti element42 = FormElementPickerMulti.createInstance().setTitle("Multi Items").setOptions(fruits).setPickerTitle("Pick one or more").setNegativeText("reset");
            FormElementSwitch element43 = FormElementSwitch.createInstance().setTitle("Frozen?").setSwitchTexts("Yes", "No");

            List<BaseFormElement> formItems = new ArrayList<>();
            formItems.add(header1);
            formItems.add(element11);
            formItems.add(element12);
            formItems.add(header2);
            formItems.add(element21);
            formItems.add(element22);
            formItems.add(element23);
            formItems.add(header3);
            formItems.add(element31);
            formItems.add(element32);
            formItems.add(element33);
            formItems.add(header4);
            formItems.add(element41);
            formItems.add(element42);
            formItems.add(element43);
            mFormBuilder.addFormElements(formItems);

        }

        @Override
        public void onValueChanged(BaseFormElement formElement) {
            Toast.makeText(this, formElement.getValue(), Toast.LENGTH_SHORT).show();
        }*/

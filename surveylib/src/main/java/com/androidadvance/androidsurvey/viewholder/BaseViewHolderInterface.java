package com.androidadvance.androidsurvey.viewholder;

import android.content.Context;

import com.androidadvance.androidsurvey.listener.FormItemEditTextListener;
import com.androidadvance.androidsurvey.model.BaseFormElement;

/**
 * Base ViewHolder method instance
 * Created by Riddhi - Rudra on 30-Jul-17.
 */

public interface BaseViewHolderInterface {
    FormItemEditTextListener getListener();
    void bind(int position, BaseFormElement formElement, Context context);
}

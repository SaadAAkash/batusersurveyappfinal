package com.interspeedcombd.surveylibtest;
/**
 * Created by interspeed.com.bd on 2/12/2018.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import com.interspeedcombd.surveylibtest.fbotp.MainFbOTP;
import com.interspeedcombd.surveylibtest.otp.MyOTPActivity;


public class Intro extends Activity
{
    int splashDelay = 1500;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro);

        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;

        //set manual height and width to the logo

        ImageView logo = (ImageView) findViewById(R.id.logo);
        logo.setMaxWidth((width*2)/3);

        // Adding a Timer to exit splash screen
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run()
            {
                //startActivity(new Intent(Intro.this, MainActivity.class));

                //startActivity(new Intent(Intro.this, MyOTPActivity.class));

                startActivity(new Intent(Intro.this, MainFbOTP.class));

                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        }, splashDelay);


    }
}
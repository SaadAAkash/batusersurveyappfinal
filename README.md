# Interspeed Survey App 

An app for surveying the valued customers of British American Tobacco (BAT) to grade them into clusters in purpose of target marketting. The questionnaires should include their habit of music, regular mode of transportation, preferred cigarrette brand, last or frequent visits to local and overseas parties/music fests and many more.

## App Features

* Basic personal information input and save them into DB
* Visual or graphical GUI for user convenience and enhanced experience
* Separate pages for each section (psychological, behaviour, music, travel, cigarrette)
* All questions must be answered, no swiping right/pressing next without filling out all the infos
* Not Applicable option for every checkbox

## App UI and specs

* Should only include black (background) and golden (text)
* Will be operated on Tabs runnng on Android OS